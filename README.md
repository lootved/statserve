# statserve


## Enable all file access
https://developer.android.com/training/data-storage/manage-all-files
Privacy > permission Manager > All files permission
MANAGE_EXTERNAL_STORAGE permission in the manifest.
Use the ACTION_MANAGE_ALL_FILES_ACCESS_PERMISSION intent action to direct users to a system settings page where they can enable the following option for your app: Allow access to manage all files.
To determine whether your app has been granted the MANAGE_EXTERNAL_STORAGE permission, call Environment.isExternalStorageManager().


## Generate platform specific bindings
```bash
  flutter create --platforms=linux,android .
  flutter create --platforms=windows,macos,linux,web,ios,android,macos .
```

## Android config
Internet permission is required in `android/app/src/main/AndroidManifest.xml` to bind to socket.
Read external storage required to serve the static files.
```xml
  <uses-permission android:name="android.permission.INTERNET"/>
  <uses-permission android:name="android.permission.MANAGE_EXTERNAL_STORAGE"/>
```

## Build apk


```bash
  flutter build apk --release
```


## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://docs.flutter.dev/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://docs.flutter.dev/cookbook)

For help getting started with Flutter development, view the
[online documentation](https://docs.flutter.dev/), which offers tutorials,
samples, guidance on mobile development, and a full API reference.


