import 'dart:io';

import 'package:file_picker/file_picker.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:shelf/shelf.dart';
import 'package:shelf/shelf_io.dart' as io;
import 'package:shelf_static/shelf_static.dart';

void main() {
  debugPrint("StatServe ==> started !");
  runApp(const MaterialApp(
    home: Home(),
  ));
}

class Home extends StatefulWidget {
  const Home({super.key});

  @override
  HomeState createState() => HomeState();
}

class HomeState extends State<Home> {
  String statusText = "Start Server";
  bool isServerStarted = false;
  int port = 8080;
  HttpServer? server;
  Color startBtnColor = Colors.green;

  String baseDir = "/storage/emulated/0/Documents/static";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          ElevatedButton(
            style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all(startBtnColor),
                padding: MaterialStateProperty.all(const EdgeInsets.all(20)),
                textStyle: MaterialStateProperty.all(
                    const TextStyle(fontSize: 14, color: Colors.white))),
            onPressed: () {
              if (isServerStarted) {
                stopServer();
              } else {
                startServer();
              }
            },
            child: Text(statusText),
          ),
          ElevatedButton(
            style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all(Colors.blue),
                padding: MaterialStateProperty.all(const EdgeInsets.all(20)),
                textStyle: MaterialStateProperty.all(
                    const TextStyle(fontSize: 14, color: Colors.white))),
            onPressed: () {
              _selectFolder();
            },
            child: Text(baseDir),
          )
        ],
      ),
    ));
  }

  void startServer() async {
    if (defaultTargetPlatform != TargetPlatform.linux) {
      var status = await Permission.manageExternalStorage.status;
      if (status.isDenied) {
        var permissionDenied = false;
        await Permission.manageExternalStorage
            .onDeniedCallback(
              () => permissionDenied = true,
            )
            .request();
        if (permissionDenied) {
          debugPrint("manage file permisison denied");
          setState(() {
            startBtnColor = Colors.red;
            statusText = "Please grant permission to manage files";
          });
          return;
        }
      }
    }
    if (!await Directory(baseDir).exists()) {
      setState(() {
        statusText = "directory $baseDir does not exists";
        startBtnColor = Colors.red;
      });
      return;
    }

    setState(() {
      statusText = "Stop server on Port : $port";
    });
    debugPrint("serving files from dir  $baseDir");
    var handler = createStaticHandler(baseDir,
        defaultDocument: 'index.html', listDirectories: true);
    // var handler =
    // const Pipeline().addMiddleware(logRequests()).addHandler(_echoRequest);
    server = await io.serve(handler, 'localhost', port);

    setState(() {
      isServerStarted = true;
      startBtnColor = Colors.red;
      statusText =
          "Server running on IP : ${server!.address.address} On Port : ${server!.port}";
    });

    debugPrint(statusText);
  }

  void stopServer() {
    server!.close();
    setState(() {
      isServerStarted = false;
      statusText = "Start Server";
      debugPrint("Stopped server");
      startBtnColor = Colors.green;
    });
  }

  Response _echoRequest(Request request) {
    debugPrint("responding to request ...");
    File f = File("$baseDir/${request.url}");
    try {
      // Read the file
      final contents = f.readAsStringSync();
      return Response.ok('Request for "${request.url}: $contents"');
    } catch (e) {
      debugPrint("err $e");
      return Response.ok('Error for "${request.url}" $e');
    }
  }

  void _selectFolder() async {
    try {
      String? path = await FilePicker.platform.getDirectoryPath(
        dialogTitle: "pick a folder",
        initialDirectory: baseDir,
        lockParentWindow: true,
      );

      if (path != null) {
        setState(() {
          // debugPrint("selected folder $path");
          baseDir = path;
        });
      }
    } on PlatformException catch (e) {
      debugPrint('Unsupported operation $e');
    } catch (e) {
      debugPrint(e.toString());
    }
  }
}
